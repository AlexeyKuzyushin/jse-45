package ru.rencredit.jschool.kuzyushin.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.rencredit.jschool.kuzyushin.tm.dto.CustomUser;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;

public interface UserUtil {

    static String getUserId() throws AccessDeniedException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException();
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }
}
