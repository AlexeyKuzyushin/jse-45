package ru.rencredit.jschool.kuzyushin.tm.exception.system;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class IncorrectArgException extends AbstractException {

    public IncorrectArgException(String value) {
        super("Error! '" + value + "' is not a Task Manager argument. See '-h'...");
    }
}
