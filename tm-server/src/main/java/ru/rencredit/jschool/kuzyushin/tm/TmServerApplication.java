package ru.rencredit.jschool.kuzyushin.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmServerApplication.class, args);
	}
}
