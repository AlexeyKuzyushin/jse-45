package ru.rencredit.jschool.kuzyushin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.CustomUser;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @Autowired
    public TaskController(
            final @NotNull IUserService userService,
            final @NotNull ITaskService taskService,
            final @NotNull IProjectService projectService
    ) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @GetMapping("")
    public ModelAndView show(@AuthenticationPrincipal final CustomUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/task-list");
        modelAndView.addObject("tasks", taskService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create(@AuthenticationPrincipal final @NotNull CustomUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/task-create");
        modelAndView.addObject("users", userService.findAll());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @ModelAttribute("task") final @NotNull Task task,
            @RequestParam(value = "projectId") final @NotNull  String projectId
    ) {
        taskService.create(user.getUserId(), projectId, task.getName(), task.getDescription());
        return new ModelAndView("redirect:/tasks");
    }

    @GetMapping("/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @PathVariable(value = "id") final @NotNull String id) {
        taskService.removeById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @PathVariable(value = "id") final @NotNull String id) {
        @Nullable final Task task = taskService.findById(user.getUserId(), id);
        return new ModelAndView("/task/task-update", "task", task);
    }

    @PostMapping("/update/{id}")
    public String update(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @ModelAttribute("task") final @NotNull Task task) {
        taskService.updateById(user.getUserId(), task.getId(), task.getName(), task.getDescription());
        return "redirect:/tasks";
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @PathVariable("id") @NotNull final String id) {
        @Nullable final Task task = taskService.findById(user.getUserId(), id);
        return new ModelAndView("/task/task-view", "task", task);
    }
}
