package ru.rencredit.jschool.kuzyushin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.CustomUser;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @Autowired
    public ProjectController(
            final @NotNull IUserService userService,
            final @NotNull IProjectService projectService,
            final @NotNull ITaskService taskService
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @GetMapping("")
    public ModelAndView show(@AuthenticationPrincipal final @NotNull CustomUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-list");
        modelAndView.addObject("projects", projectService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create(@AuthenticationPrincipal final @NotNull CustomUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-create");
        modelAndView.addObject("users", userService.findAll());
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @ModelAttribute("project") final @NotNull Project project
    ) {
        projectService.create(user.getUserId(), project.getName(), project.getDescription());
        return new ModelAndView("redirect:/projects");
    }

    @GetMapping("/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @PathVariable(value = "id") final @NotNull String id) {
        projectService.removeById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @PathVariable(value = "id") final @NotNull String id) {
        @Nullable final Project project = projectService.findById(user.getUserId(), id);
        return new ModelAndView("/project/project-update", "project", project);
    }

    @PostMapping("/update/{id}")
    public String update(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @ModelAttribute("project") final @NotNull Project project) {
        projectService.updateById(user.getUserId(), project.getId(),
                project.getName(), project.getDescription());
        return "redirect:/projects";
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(
            @AuthenticationPrincipal final @NotNull CustomUser user,
            @PathVariable("id") @NotNull final String id) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-view");
        modelAndView.addObject("project", projectService.findById(user.getUserId(), id));
        modelAndView.addObject("tasks", taskService.findAllByProjectId(id));
        return modelAndView;
    }
}
