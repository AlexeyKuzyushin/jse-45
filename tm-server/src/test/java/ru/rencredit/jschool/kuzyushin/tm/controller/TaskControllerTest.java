package ru.rencredit.jschool.kuzyushin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.TmServerApplication;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TmServerApplication.class)
public class TaskControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    private final static String baseUrl = "/tasks";

    private static final String adminCredentials = "adminTest";

    private static final User admin = new User();

    private static final Project projectOne = new Project();

    private static final Project projectTwo = new Project();

    private static final Task taskOne = new Task();

    private static final Task taskTwo = new Task();

    @BeforeClass
    public static void set() {
        projectOne.setName("projectOne");
        projectOne.setDescription("projectOne");
        projectOne.setUser(admin);

        projectTwo.setName("projectTwo");
        projectTwo.setDescription("projectTwo");
        projectTwo.setUser(admin);

        taskOne.setName("taskOne");
        taskOne.setDescription("taskOne");
        taskOne.setProject(projectOne);
        taskOne.setUser(admin);

        taskTwo.setName("taskTwo");
        taskTwo.setDescription("taskTwo");
        taskTwo.setProject(projectTwo);
        taskTwo.setUser(admin);
    }

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        admin.setLogin(adminCredentials);
        admin.setPasswordHash(passwordEncoder.encode(adminCredentials));
        userRepository.save(admin);
        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);

        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(adminCredentials, adminCredentials);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void clear() {
        userRepository.deleteAll();
        projectRepository.deleteAll();
        taskRepository.deleteAll();
    }

    @Test
    public void showTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/task-list"));
    }

    @Test
    public void createGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/task-create"));
    }

    @Test
    public void createPostTest() throws Exception {
        @NotNull final Task task = new Task();
        task.setName("test");
        task.setDescription("test");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create").param("projectId", projectOne.getId())
                .flashAttr("task", task))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tasks"))
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void deleteTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/delete/{id}", taskOne.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tasks"))
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void updateGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/update/{id}", taskOne.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/task-update"));
    }

    @Test
    public void updatePostTest() throws Exception {
        @NotNull final Task task = new Task();
        task.setName("other-name");
        task.setDescription("other-description");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/update/{id}", taskOne.getId())
                .flashAttr("task", task))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tasks"))
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void viewTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/view/{id}", taskOne.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/task-view"));
    }
}
