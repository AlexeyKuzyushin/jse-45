package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractListener {

    @NotNull
    private final ProjectSoapEndpoint projectSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public ProjectCreateListener(
            final @NotNull ProjectSoapEndpoint projectSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.projectSoapEndpoint = projectSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER USER-ID]");
        @Nullable final String userId = TerminalUtil.nextLine();
        @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(userId);
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        sessionService.setListCookieRowRequest(projectSoapEndpoint);
        projectSoapEndpoint.createProject(projectDTO);
        System.out.println("[OK]");
    }
}
