package ru.rencredit.jschool.kuzyushin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;

@Component
public final class HelpListener extends AbstractListener {

//    @NotNull
//    private List<AbstractListener> commandList;
//
//    @Autowired
//    public HelpListener(final @NotNull List<AbstractListener> commandList) {
//        this.commandList = commandList;
//    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands";
    }

    @Override
    @EventListener(condition = "@helpListener.name() == #event.command || (@helpListener.arg() == #event.command)")
    public void handler(final ConsoleEvent event) {
        System.out.println("[HELP]");
//        for (@NotNull final AbstractListener command: commandList) {
//            StringBuilder result = new StringBuilder();
//            command.name();
//            if (!command.name().isEmpty()) result.append(command.name());
//            if (command.arg() != null && !command.arg().isEmpty()) result.append(", ").append(command.arg());
//            command.description();
//            if (!command.description().isEmpty())
//                result.append(": ").append(command.description());
//            System.out.println(result.toString());
//        }
//        System.out.println("[OK]");
    }
}
