package ru.rencredit.jschool.kuzyushin.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.ClientBootstrap;

@SpringBootApplication
public class TmClientSoapApplication implements CommandLineRunner {

	@Autowired
	private ApplicationContext applicationContext;

	public static void main(String[] args) {
		SpringApplication.run(TmClientSoapApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		@NotNull final ClientBootstrap clientBootstrap = applicationContext.getBean(ClientBootstrap.class);
		clientBootstrap.run(args);
	}
}
